import functools

with open('data.txt') as f:
    lines = f.read().splitlines()

@functools.cache
def all_combinations(string):
  """
  Generates a list of all possible combinations of a string, replacing only question marks ('?') with either a period ('.') or a hashtag ('#').

  Args:
    string: The input string.

  Returns:
    A list of strings.
  """

  if not string:
    return [""]

  if string == "?":
    return [".", "#"]

  first_char = string[0]
  remaining_string = string[1:]

  first_combinations = all_combinations(remaining_string)

  new_combinations = []
  for first_combination in first_combinations:
    if first_char == "?":
      new_combinations.append("." + first_combination)
      new_combinations.append("#" + first_combination)
    else:
      new_combinations.append(first_char + first_combination)

  return new_combinations

def valid(str, nums) -> bool:
  parts = []
  for x in str.split('.'):
    if x:
      parts.append(x)
  if len(parts) != len(nums):
    return False
  for i, part in enumerate(parts):
    if len(part) != nums[i]:
      return False
  return True

# adjust input for part 2
for i, line in enumerate(lines):
  l = line.split()[0]
  newline = f"{l}?{l}?{l}?{l}?{l} "
  r = line.split()[1]
  newline += f"{r},{r},{r},{r},{r}"
  lines[i] = newline

sum = 0
for line in lines:
  combinations = all_combinations(line.split()[0])
  print("found combinations", len(combinations))
  valids = 0
  for c in combinations:
    v = valid(c, [int(x) for x in line.split()[1].split(",")])
    # print(f"{line} --- {c} --- {v}")
    if v:
      valids += 1
  print(line, "---", valids)
  sum += valids

print(sum)