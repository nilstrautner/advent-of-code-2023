import re

with open('data.txt') as f:
    lines = f.read().splitlines()

def task1():
    red, green, blue = 12, 13, 14

    sumIds = 0
    for line in lines:
        id = int(line.split(':')[0].split(' ')[1])
        print(id)
        tooHigh = False
        
        sets = line.split(':')[1][1:].split('; ')
        for set in sets:
            moves = set.split(', ')
            for move in moves:
                if "red" in move:
                    i = int(move.split(' ')[0])
                    # print(f"RED ", i)
                    if i > red:
                        tooHigh = True
                if "green" in move:
                    i = int(move.split(' ')[0])
                    # print(f"GREEN ", i)
                    if i > green:
                        tooHigh = True
                if "blue" in move:
                    i = int(move.split(' ')[0])
                    # print(f"BLUE ", i)
                    if i > blue:
                        tooHigh = True
        
        if tooHigh:
            print(f"{line} tooHigh? {tooHigh}")

        if not tooHigh:
            sumIds += id
    print("Sum", sumIds)

def task2():
    sumPower = 0
    for line in lines:
        highestRed, highestGreen, highestBlue = 0, 0, 0
        
        sets = line.split(':')[1][1:].split('; ')
        for set in sets:
            moves = set.split(', ')
            for move in moves:
                if "red" in move:
                    i = int(move.split(' ')[0])
                    if i > highestRed:
                        highestRed = i
                if "green" in move:
                    i = int(move.split(' ')[0])
                    if i > highestGreen:
                        highestGreen = i
                if "blue" in move:
                    i = int(move.split(' ')[0])
                    if i > highestBlue:
                        highestBlue = i
        sumPower += (highestRed*highestGreen*highestBlue)
    print("Sum", sumPower)

task2()