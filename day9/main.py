with open('data.txt') as f:
    lines = f.readlines()

sum = 0
for line in lines:
    layers = []
    layers.append([int(x) for x in line.split()])
    layers[-1] = list(reversed(layers[-1]))

    layer_id = 0
    while True:
        # build down
        numbers = []
        for i in range(len(layers[layer_id])-1):
            numbers.append( layers[layer_id][i+1]-layers[layer_id][i] )
        
        layers.append(numbers)
        layer_id += 1

        # check end
        end = True
        for x in numbers:
            if x != 0:
                end = False
        if end:
            break

    # build end
    print(list(reversed(range(0, len(layers)))))
    for i in reversed(range(0, len(layers))):
        if i == len(layers)-1:
            layers[i].append(0)
            continue
        layers[i].append( layers[i][-1] + layers[i+1][-1] )
    print(layers)
        
    sum += layers[0][-1]


print(sum)