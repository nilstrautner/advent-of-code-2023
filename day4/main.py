with open('data.txt') as f:
    lines = f.read().splitlines()

def task1():
    sum = 0

    for line in lines:
        # win nrs
        win_nrs_str = line.split(": ")[1].split(" | ")[0].split()
        win_nrs = [int(x) for x in win_nrs_str if x]
        # print("Win nrs", win_nrs )

        # my nrs
        my_nrs_str = line.split(": ")[1].split(" | ")[1].split()
        my_nrs = [int(x) for x in my_nrs_str if x]
        # print("My nrs", my_nrs )

        # compare
        line_sum = 0
        for x in my_nrs:
            if x in win_nrs:
                # first time
                if line_sum == 0:
                    line_sum = 1
                else:
                    line_sum *= 2
        # print("Line sum", line_sum)

        sum += line_sum
    
    print("Total sum", sum)


def task2():
    # card_matches[card-1] = #matches
    card_matches = []
    for line in lines:
        # win nrs
        win_nrs_str = line.split(": ")[1].split(" | ")[0].split()
        win_nrs = [int(x) for x in win_nrs_str if x]

        # my nrs
        my_nrs_str = line.split(": ")[1].split(" | ")[1].split()
        my_nrs = [int(x) for x in my_nrs_str if x]

        # compare
        i = 0
        for x in my_nrs:
            if x in win_nrs:
                i += 1
        card_matches.append(i)
    
    # card_amount[card-1] = #matches
    card_amount = [1 for i in card_matches]
    for i, x in enumerate(card_amount):
        matches = card_matches[i]
        for m in range(matches):
            i_target = i+m+1
            if i_target >= len(card_amount):
                break
            card_amount[i_target] += card_amount[i]
        # print("Card", i, card_amount)
    
    # sum
    print( sum(card_amount) )
    

task2()