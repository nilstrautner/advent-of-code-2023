import sys
from functools import *
sys.setrecursionlimit(100000)

with open('data.txt') as f:
    lines = f.read().splitlines()

visited = set() # (x,y, dir)
result = set()

def display():
   for y, line in enumerate(lines):
      ll = ""
      for x, char in enumerate(line):
         if (x, y) in result:
            ll += "#"
         else:
            ll += char
      print(ll)


# @cache
def move(pos: tuple, dir: str, alive=0):
   if pos[0] < 0 or pos[1] < 0 or pos[0] >= len(lines[0]) or pos[1] >= len(lines):
      # print("Light lost outside of grid")
      return
   # print("Light valid at", pos[0], pos[1], "Dirrection:", dir, "Char:", lines[pos[1]][pos[0]])

   if (pos[0], pos[1], dir) in visited:
      # print("Light pos already visited")
      return
   visited.add((pos[0], pos[1], dir))

   # display()

   char = lines[pos[1]][pos[0]]
   if char == '\\':
      if dir == "RIGHT":
         move((pos[0], pos[1]+1), "DOWN", alive)
      if dir == "LEFT":
         move((pos[0], pos[1]-1), "TOP", alive)
      if dir == "TOP":
         move((pos[0]-1, pos[1]), "LEFT", alive)
      if dir == "DOWN":
         move((pos[0]+1, pos[1]), "RIGHT", alive)

   if char == '/':
      if dir == "RIGHT":
         move((pos[0], pos[1]-1), "TOP", alive)
      if dir == "LEFT":
         move((pos[0], pos[1]+1), "DOWN", alive)
      if dir == "TOP":
         move((pos[0]+1, pos[1]), "RIGHT", alive)
      if dir == "DOWN":
         move((pos[0]-1, pos[1]), "LEFT", alive)

   if char == '.':
      if dir == "RIGHT":
         move((pos[0]+1, pos[1]), dir, alive)
      if dir == "LEFT":
         move((pos[0]-1, pos[1]), dir, alive)
      if dir == "TOP":
         move((pos[0], pos[1]-1), dir, alive)
      if dir == "DOWN":
         move((pos[0], pos[1]+1), dir, alive)

   if char == '-':
      if dir == "RIGHT":
         move((pos[0]+1, pos[1]), dir, alive)
      if dir == "LEFT":
         move((pos[0]-1, pos[1]), dir, alive)
      if dir == "TOP" or dir == "DOWN":
         # print("Separated")
         move((pos[0]-1, pos[1]), "LEFT", alive)
         move((pos[0]+1, pos[1]), "RIGHT", alive)
   
   if char == '|':
      if dir == "TOP":
         move((pos[0], pos[1]-1), dir, alive)
      if dir == "DOWN":
         move((pos[0], pos[1]+1), dir, alive)
      if dir == "LEFT" or dir == "RIGHT":
         # print("Separated")
         move((pos[0], pos[1]+1), "DOWN", alive)
         move((pos[0], pos[1]-1), "TOP", alive)

def part1():
   move((0, 0), "RIGHT")

   result.clear()
   for v in visited:
      result.add((v[0], v[1]))
   display()
   print("Visited:", len(result))
   
def part2():
   highest = 0

   # first row down
   # print("Shooting down")
   for x, _ in enumerate(lines[0]):
      visited.clear()
      result.clear()
      move((x, 0), "DOWN")
      for v in visited:
         result.add((v[0], v[1]))
      # print("Result", len(result))
      if len(result) > highest:
         highest = len(result)

   # last row up
   # print("Shooting up")
   for x, _ in enumerate(lines[0]):
      visited.clear()
      result.clear()
      move((x, len(lines)-1), "TOP")
      for v in visited:
         result.add((v[0], v[1]))
      # print("Result", len(result))
      if len(result) > highest:
         highest = len(result)
   
   # left column right
   # print("Shooting right")
   for y, _ in enumerate(lines):
      visited.clear()
      result.clear()
      move((0, y), "RIGHT")
      for v in visited:
         result.add((v[0], v[1]))
      # print("Result", len(result))
      if len(result) > highest:
         highest = len(result)

   # right column left
   # print("shooting left")
   for y, line in enumerate(lines):
      visited.clear()
      result.clear()
      move((len(line)-1, y), "LEFT")
      for v in visited:
         result.add((v[0], v[1]))
      # print("Result", len(result))
      if len(result) > highest:
         highest = len(result)

   print("Highest:", highest)
part2()