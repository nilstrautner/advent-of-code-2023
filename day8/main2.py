import re
import numpy as np

with open('data.txt') as f:
    lines = f.read().splitlines()

order = lines[0]

graph = dict()
currents = []

# build graph
lines = lines[2:]
for line in lines:
    now = line.split(" = ")[0]
    left = line.split(" = ")[1].split(", ")[0].replace("(", "")
    right = line.split(" = ")[1].split(", ")[1].replace(")", "")
    # print(now, left, right)
    graph[now] = (left, right)
    if now[2] == "A":
        currents.append(now)

# get cycles
startToZ = []
for current in currents:
    i = 0
    count = 0
    pos = current

    # startToZ
    while pos[2] != "Z":
        dir = order[i%len(order)]
        if dir == "L":
            pos = graph[pos][0]
        if dir == "R":
            pos = graph[pos][1]

        i += 1
        count += 1
        # print(current, pos)
    print("found", pos)
    
    # print(current, "startToZ:", count)
    startToZ.append(count)
    

print(startToZ)

result = np.lcm.reduce(np.array(startToZ, dtype=np.int64))

print("z", result)