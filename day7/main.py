import re

with open('data.txt') as f:
    lines = f.read().splitlines()

chars = ['A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2', 'J']
valued = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm']

def get_kind1(cards: str) -> int:
    # five of a kind 
    for c in chars:
        if cards.count(c) == 5:
            return 0
    # four of a kind
    for c in chars:
        if cards.count(c) == 4:
            return 1
    # full house
    for c in chars:
        if cards.count(c) == 3:
            for c in chars:
                if cards.count(c) == 2:
                    return 2
    # three of a kind
    for c in chars:
        if cards.count(c) == 3:
            return 3
    # two pairs
    for c in chars:
        if cards.count(c) == 2:
            rest = cards.replace(c,'')
            for c in chars:
                if rest.count(c) == 2:
                    return 4
    # one pair
    for c in chars:
        if cards.count(c) == 2:
            return 5
    # high card
    return 6

def part1():
    # get kind
    table = dict()
    for line in lines:
        cards = line.split()[0]
        points = line.split()[1]

        # print(cards, get_kind(cards))
        table[cards] = (get_kind1(cards), points)

    def get_value(cards) -> str:
        d = ''
        for c in cards:
            d += valued[chars.index(c)]
        # print(d)
        return d

    # sort
    table = dict(sorted(table.items(), key=lambda item: (item[1][0], get_value(item[0]))))
    # print(table)

    for t in table:
        print(t, table[t])

    # calc
    id = len(table)
    sum = 0
    for cards in table:
        points = int(table[cards][1])
        sum += id * points
        id -= 1

    print(sum)

def get_kind2(original_cards: str) -> int:
    values = []
    cards = original_cards
    joker_indexes = [m.start() for m in re.finditer('J', cards)]  # [2, 4]
    card_indexes_per_joker = [0 for _ in joker_indexes]  # [0, 0]
    print(joker_indexes, "indexes")
    values.append( get_kind1(cards) )
    print("testing card", cards)
    if joker_indexes and values[0] != 0:

        i = 0
        while ( i < len(joker_indexes) ):
                # replace symbol
                card_list = list(original_cards)
                for j in range(len(joker_indexes)):
                    card_list[joker_indexes[j]] = chars[card_indexes_per_joker[j]]
                    # print(j, joker_indexes[j], card_indexes_per_joker[j], cards[joker_indexes[j]], chars[card_indexes_per_joker[j]])
                cards = "".join(card_list)

                # print("...Testing", cards)
                values.append( get_kind1(cards) )
                
                # print(card_indexes_per_joker)
                card_indexes_per_joker[i] += 1

                if card_indexes_per_joker[i] >= len(chars):
                    card_indexes_per_joker[i] = 0
                    if i+1 >= len(joker_indexes):
                        break
                    card_indexes_per_joker[i+1] += 1 

                    if card_indexes_per_joker[i+1] >= len(chars):
                        card_indexes_per_joker[i+1] = 0
                        if i+2 >= len(joker_indexes):
                            break
                        card_indexes_per_joker[i+2] += 1 

                        if card_indexes_per_joker[i+2] >= len(chars):
                            card_indexes_per_joker[i+2] = 0
                            if i+3 >= len(joker_indexes):
                                break
                            card_indexes_per_joker[i+3] += 1 

                            if card_indexes_per_joker[i+3] >= len(chars):
                                card_indexes_per_joker[i+3] = 0
                                if i+4 >= len(joker_indexes):
                                    break
                                card_indexes_per_joker[i+4] += 1 
                
                
        
    
    values.sort()
    return values[0]

def part2():
    # get kind
    table = dict()
    for line in lines:
        cards = line.split()[0]
        points = line.split()[1]

        # print(cards, get_kind2(cards))
        table[cards] = (get_kind2(cards), points)

    # for t in table:
    #     print(t, table[t])

    def get_value(cards) -> str:
        d = ''
        for c in cards:
            d += valued[chars.index(c)]
        # print(d)
        return d

    # sort
    table = dict(sorted(table.items(), key=lambda item: (item[1][0], get_value(item[0]))))
    # print(table)

    for t in table:
        print(t, table[t])

    # calc
    id = len(table)
    sum = 0
    for cards in table:
        points = int(table[cards][1])
        sum += id * points
        id -= 1

    print(sum)

part2()