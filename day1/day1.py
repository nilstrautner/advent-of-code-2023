import re

with open('data.txt') as f:
    lines = f.read().splitlines()

def task1():
    sum = 0
    for line in lines:
        for char in line:
            if char.isdigit():
                number1 = char
                break

        for char in reversed(line):
            if char.isdigit():
                number2 = char
                break
        
        number = str(number1) + str(number2)
        number = int(number)
        print(number)
        sum += number

    print("Task 1: ", sum)
    
def task2():
    sum = 0
    words = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
    for line in lines:
        # number 1
        indexWord = 1000000
        for word in words:
            if word in line:
                i = line.index(word)
                if i < indexWord:
                    indexWord = i
                    number1 = word

        for i, char in enumerate(line):
            if char.isdigit():
                if i < indexWord:
                    number1 = char
                break
        

        # number 2
        indexWord = -10000
        for word in words:
            highestIndexes = [m.start() for m in re.finditer(word, line)]
            if highestIndexes:
                highestIndex = highestIndexes[-1]
                if highestIndex > indexWord:
                    indexWord = highestIndex
                    number2 = word

        print(f"last word ({number2}) is at index {indexWord}")
        for i, char in enumerate(line):
            if char.isdigit():
                if i > indexWord:
                    number2 = char
                    indexWord = i #?

        print(f"last digit ({number2}) is at index {indexWord}")

        # putting it together
        def to_number(input: str):
            if input not in words:
                return int(input)
            return words.index(input) + 1
            
        number1 = to_number(number1)
        number2 = to_number(number2)

        number = str(number1) + str(number2)
        number = int(number)

        print(f"{line}: {number1} {number2} -> {number}")

        sum += number

    print("Task 2: ", sum)

task2()