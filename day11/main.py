import itertools


with open('data.txt') as f:
    lines = f.read().splitlines()

# expand galaxy
emptyRows, emptyColumns = [], []
galaxies = []
for y, line in enumerate(lines):
    isEmpyRow = True
    for x, char in enumerate(line):
        if char == '#':
            isEmpyRow = False
            galaxies.append((x, y))
    if isEmpyRow:
        emptyRows.append(y)

for x in range(len(lines[0])):
    for y, line in enumerate(lines):
        isEmptyColumns = True
        if lines[y][x] == '#':
            isEmptyColumns = False
            break
    if isEmptyColumns:
        emptyColumns.append(x)

# print("Empty rows", emptyRows)
# print("Empty columns", emptyColumns)
# print("Galaxies:", galaxies)
n = 1000000
for i, g in  enumerate(galaxies):
    x, y = g[0], g[1]
    for r in emptyRows:
        if r < g[1]:
            y += n-1
    for c in emptyColumns:
        if c < g[0]:
            x += n-1
    galaxies[i] = (x, y)
print("Galaxies after expansion:", galaxies)

# shortest path
sum = 0
pairs = list(itertools.combinations(galaxies, 2))
for pair in pairs:
    dx = abs(pair[0][0]-pair[1][0])
    dy = abs(pair[0][1]-pair[1][1])
    sum += dx+dy
    # print(pair, dx+dy)

print("Sum:",sum)
