

with open('data.txt') as f:
    lines = f.read().splitlines()

def hash(str: str) -> int:
   current = 0
   for char in str:
      ascii = ord(char)
      current += ascii
      current = current * 17
      current = current % 256
   return current

sum = 0
line = lines[0].strip()
parts = line.split(',')
boxes = {}
for part in parts:
   # remove
   if '-' in part:
      label = part.replace('-', '')
      box = hash(label)
      
      if box in boxes:
         i = -1
         for ii, l in enumerate(boxes[box]):
            if label == l.split('=')[0]:
               i = ii
               break
         if i != -1:
            boxes[box].pop(i)

   # add
   else:
      label = part.split('=')[0]
      focal = part.split('=')[1]
      box = hash(label)
      if box not in boxes:
         boxes[box] = []

      i = -1
      for ii, l in enumerate(boxes[box]):
         if label == l.split('=')[0]:
            i = ii
            print("ee")
            break
      if i != -1:
         boxes[box][i] = part
      else:
         boxes[box].append(part)
   
   print("After", part)
   print(boxes)

# calc
sum = 0
for box in boxes:
   l = boxes[box]
   for i, item in enumerate(l):
      v = (int(box)+1) * (i+1) * int(item.split("=")[1])
      print(v)
      sum += v

print("Sum", sum)