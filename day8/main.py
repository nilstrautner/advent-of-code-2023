import re

with open('data.txt') as f:
    lines = f.read().splitlines()

order = lines[0]

class Node:
    def __init__(self, id, left, right):
        self.id = id
        self.left = left
        self.right = right
node = None

graph = dict()

# build graph
lines = lines[2:]
for line in lines:
    now = line.split(" = ")[0]
    left = line.split(" = ")[1].split(", ")[0].replace("(", "")
    right = line.split(" = ")[1].split(", ")[1].replace(")", "")
    # print(now, left, right)
    graph[now] = (left, right)

# count
current = "AAA"
i = 0
count = 0
while current != "ZZZ":
    dir = order[i%len(order)]
    if dir == "L":
        current = graph[current][0]
    if dir == "R":
        current = graph[current][1]
    i += 1
    count += 1
    # print("current node", current)
print("Count", count)