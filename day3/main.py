with open('data.txt') as f:
    lines = f.read().splitlines()

def task1():
    sum = 0

    for y, line in enumerate(lines):
        number = ""
        valid = False
        for x, char in enumerate(line):
            if lines[y][x].isdigit():
                # print("Checking digit ", char, "at", x, y)
                number += char

                # check surroundings
                for xx in range(-1, 2):
                    for yy in range(-1, 2):
                        y_check, x_check = y+yy, x+xx
                        if y_check < 0 or y_check >= len(lines) or x_check < 0 or x_check >= len(line):
                            continue
                        
                        #print(f"Checking {lines[y][x]} with relative {xx}, {yy}") 
                        if not lines[y_check][x_check].isdigit() and not lines[y_check][x_check] == '.':
                            # print(f"{lines[y][x]} ({x},{y}) is valid digit, because of adjacent {lines[y_check][x_check]} at {x_check},{y_check} with relative {xx},{yy}") 
                            # print("Line", y_check, lines[y_check])
                            valid = True
            
            if not lines[y][x].isdigit() or x == len(line)-1:
                if valid:
                    sum += int(number)
                    print(f"{number} is valid number")
                else:
                    if number:
                        print(f"{number} is not valid number at {x},{y}")

                number = ""
                valid = False

    print("Sum", sum)

def task2():
    gears = {} 

    for y, line in enumerate(lines):
        number = ""
        valid = False
        gearList = []
        for x, char in enumerate(line):
            if lines[y][x].isdigit():
                # print("Checking digit ", char, "at", x, y)
                number += char

                # check surroundings
                for xx in range(-1, 2):
                    for yy in range(-1, 2):
                        y_check, x_check = y+yy, x+xx
                        if y_check < 0 or y_check >= len(lines) or x_check < 0 or x_check >= len(line):
                            continue
                        
                        #print(f"Checking {lines[y][x]} with relative {xx}, {yy}") 
                        if not lines[y_check][x_check].isdigit() and not lines[y_check][x_check] == '.':
                            # print(f"{lines[y][x]} ({x},{y}) is valid digit, because of adjacent {lines[y_check][x_check]} at {x_check},{y_check} with relative {xx},{yy}") 
                            # print("Line", y_check, lines[y_check])
                            if lines[y_check][x_check] == '*':
                                gearList.append((x_check , y_check) )
                            valid = True
            
            if not lines[y][x].isdigit() or x == len(line)-1:
                if valid:
                    for g in gearList:
                        if g not in gears:
                            gears[g] = set()
                        gears[ g ].add(int(number))

                number = ""
                valid = False
                gearList = []

    # count gears
    print(gears)
    sum = 0
    for g in gears:
        gg = list(gears[g])
        if len(gg) < 2:
            continue
        sum += gg[0]*gg[1]


    print("Sum", sum)

task2()