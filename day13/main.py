

with open('data.txt') as f:
    patterns = f.read().splitlines()

# Split input in multiple patterns
temp = []
linesPattern = []
for l in patterns:
    if l == "\n" or not l:
       linesPattern.append(temp)
       temp = []
       continue
    temp.append(l)
linesPattern.append(temp)

def get_col(lines, i) -> str:
   return "".join([x[i] for x in lines])

columns = []
rows = []

def find(lines):
  # find mirrow rows
  for i in range(len(lines)):
    # print("Checking ", i)
    mirrors = True
    for ii in range(1, len(lines)):
        if i-ii < 0 or i+ii < 0 or i-ii >= len(lines) or i+ii+1 >= len(lines):
          continue
        if lines[i+1+ii] != lines[i-ii]:
          mirrors = False
          # print("Doesnt mirror!", lines[i+ii], lines[i-ii])
    if mirrors and i < len(lines)-1 and lines[i] == lines[i+1]:
      print("Mirror line found", i)
       # rows.append(i+1)
      return (i+1)*100

  # find mirrow columns
  for i in range(len(lines[0])):
    # print("Checking ", i)
    mirrors = True
    for ii in range(1, len(lines[0])):
        if i-ii < 0 or i+ii < 0 or i-ii >= len(lines[0]) or i+ii+1 >= len(lines[0]):
          continue
        if get_col(lines, i+1+ii) != get_col(lines, i-ii):
          mirrors = False
          # print("Doesnt mirror!", i, ii, get_col(lines, i+1+ii), get_col(lines, i-ii))
    if mirrors:
       print("Possible column mirrow found", i)
    if mirrors and i < len(lines[0])-1 and get_col(lines, i) == get_col(lines, i+1):
      print("Mirror column found", i)
      # columns.append(i+1)
      return i+1
      
      
sum = 0
for patternid, lines in enumerate(linesPattern):
  print("Pattern", patternid)
  old = find(lines)
  print("Checking smudge")
  # sum += old

  # replace symbol
  found = False
  for y, line in enumerate(lines):
     for x, char in enumerate(lines[y]):
        if found:
           continue
        new = '.'
        if char == '.':
          new = '#'
        linesCopy = lines
        lineList = list(line)
        lineList[x] = new
        linesCopy[y] = "".join(lineList)
        n = find(linesCopy)
        if n and n != old:
           print("Found different tile", x, y)
           sum += n
           found = True

# Calc result
# print("Columns", columns, "Len:", len(columns), "Sum:", sum(columns))
# print("Rows", rows, "Len:", len(rows), "Sum:", sum(rows))

# result = sum(columns) + 100 * sum(rows)
print("Result", sum)