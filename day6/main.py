

with open('data.txt') as f:
    lines = f.read().splitlines()

def task1():
    times = [int(x) for x in lines[0].split(":")[1].split()]
    distances = [int(x) for x in lines[1].split(":")[1].split()]

    multipliedWinningGames = 1
    for game in range(len(times)):
        winningGamesAmount = 0
        for i in range(times[game]):
            speed = i
            timeLeft = times[game]-i
            distanceTraveled = speed * timeLeft
            # print(game, i, distanceTraveled, distanceTraveled > distances[game])
            if distanceTraveled > distances[game]:
                winningGamesAmount += 1
        multipliedWinningGames *= winningGamesAmount
        print(multipliedWinningGames)

def task2():
    times = int(lines[0].split(":")[1].replace(' ', ''))
    distances = int(lines[1].split(":")[1].replace(' ', ''))

    print(times, distances)
    multipliedWinningGames = 1
    winningGamesAmount = 0
    for i in range(times):
        speed = i
        timeLeft = times-i
        distanceTraveled = speed * timeLeft
        # print(game, i, distanceTraveled, distanceTraveled > distances[game])
        if distanceTraveled > distances:
            winningGamesAmount += 1
    multipliedWinningGames *= winningGamesAmount
    print(multipliedWinningGames)

task2()