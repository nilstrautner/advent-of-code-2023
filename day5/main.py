with open('data.txt') as f:
    lines = f.read().splitlines()

def task2():
    list = []
    
    # Starting seeds range
    seeds = []
    s = lines[0].split(": ")[1].split()
    i = 0
    while i < len(s):
        seeds.append( (s[i], s[i+1]) )
        i += 2
    print("Starting Seeds", seeds)
    
    def seed_to_next_stage(list):
        for i, x in enumerate(seeds):
            
            for ii, xx in enumerate(list):
                if x >= xx[1] and x < xx[1]+xx[2]:
                    seeds[i] = x - xx[1] + xx[0]

        # print("Seeds", seeds)     
    
    current_dir = -1
    for line in lines[2:]:
        # print(line)

        if not line or line == "\n":
            # print(dir)
            seed_to_next_stage(list)
            list = []
            continue

        if "-to-" in line.split()[0]:
            current_dir += 1
            print(line)
            continue
        
        list.append( (int(line.split()[0]), int(line.split()[1]), int(line.split()[2])) )

    seed_to_next_stage(list)
    print("Smallest location:", min(seeds) )

task2()