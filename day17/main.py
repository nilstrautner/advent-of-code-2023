import sys
from functools import *

with open('data.txt') as f:
    lines = f.read().splitlines()
grid = [list(map(int, line.strip())) for line in lines]

start = (0, 0)


# init
dist = dict()
prev = dict()
Q = []
for y in range(len(grid)):
   for x in range(len(grid[y])):
      dist[(x,y)] = 10000000000
      prev[(x,y)] = None
      Q.append((x,y))
dist[start] = 0

seen = dict() # ((dirx, diry), n)
seen[start] = [((0, 1), 1)]

# dijkstra
while Q:
   # smallest dist
   smallest, u = 10000000, None
   for pos in dist:
      if pos not in Q:
         continue
      if dist[pos] < smallest:
         smallest = dist[pos]
         u = pos
   Q.remove(u)

   # each neighbour
   for dir in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
      v = (u[0]+dir[0], u[1]+dir[1])
      print(seen)
      if u in seen:
         for tuple in seen[u]:
            if tuple[1]+1 > 4:
               print("Too far!")
               continue

      if not u in seen:
         seen[u] = []
         seen[u].append( (dir, 1) )
      else:
         for i, tuple in enumerate(seen[u]):
            if tuple[0] == dir:
               seen[u][i] = (seen[u][i][0], seen[u][i][1]+1)

      if v in Q:
         alternative = dist[u] + grid[v[1]][v[0]]
         if alternative < dist[v]:
            dist[v] = alternative
            prev[v] = u

# calc shortest path
target = (len(grid[0])-1, len(grid)-1)
way = [target]
u = target
while prev[u]:
   u = prev[u]
   way.insert(0, u)

# print
for y in range(len(grid)):
   ll = ""
   for x in range(len(grid[y])):
      if (x, y) in way:
         ll += "X"
      else:
         ll += str(grid[y][x])
   print(ll)