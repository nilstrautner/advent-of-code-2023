with open('data.txt') as f:
    lines = f.readlines()
w, h = len(lines[0]), len(lines)

# find start position
start_x, start_y = -1, -1
for y, line in enumerate(lines):
    if 'S' in line:
        start_x= line.index('S')
        start_y = y
print("Starting position", start_x, start_y)

visited = []

def is_inside(tuple):
    if tuple[0] < 0 or tuple[0] >= w or tuple[1] < 0 or tuple[1] >= h:
        return False
    return True

# check surroundings
x, y = start_x, start_y
distance = 0
while distance == 0 or lines[y][x] != 'S':
    if lines[y][x] == '.':
        print("Error!")
        exit(1)
            
    distance += 1
    visited.append( (x, y) )
    # print("Now at", lines[y][x], x, y)

    if lines[y][x] == 'S':
        if lines[y][x+1] != '.':
            x += 1
            continue
        if lines[y][x-1] != '.':
            x -= 1
            continue
        if lines[y+1][x] != '.':
            y += 1
            continue
        if lines[y-1][x] != '.':
            y -= 1
            continue
    
    else:
        if distance == 3:
            visited.remove( (start_x, start_y) )
        if lines[y][x] == '-':
            next1, next2 = (x+1, y), (x-1, y)
            if not next1 in visited and is_inside(next1):
                x = next1[0]
            elif next2 not in visited and is_inside(next2):
                x = next2[0]
            else:
                print("Error! Both path options are already visited!")
                exit(1)
            continue
        if lines[y][x] == '|':
            next1, next2 = (x, y+1), (x, y-1)
            if not next1 in visited and is_inside(next1):
                y = next1[1]
            elif next2 not in visited and is_inside(next2):
                y = next2[1]
            else:
                print("Error! Both path options are already visited!")
                exit(1)
            continue
        if lines[y][x] == '7':
            next1, next2 = (x-1, y), (x, y+1)
            if not next1 in visited and is_inside(next1):
                x = next1[0]
            elif next2 not in visited and is_inside(next2):
                y = next2[1]
            else:
                print("Error! Both path options are already visited!")
                exit(1)
            continue
        if lines[y][x] == 'J':
            next1, next2 = (x-1, y), (x, y-1)
            if not next1 in visited and is_inside(next1):
                x = next1[0]
            elif next2 not in visited and is_inside(next2):
                y = next2[1]
            else:
                print("Error! Both path options are already visited!")
                exit(1)
            continue
        if lines[y][x] == 'L':
            next1, next2 = (x, y-1), (x+1, y)
            if not next1 in visited and is_inside(next1):
                y = next1[1]
            elif next2 not in visited and is_inside(next2):
                x = next2[0]
            else:
                print("Error! Both path options are already visited!")
                exit(1)
            continue
        if lines[y][x] == 'F':
            next1, next2 = (x+1, y), (x, y+1)
            if not next1 in visited and is_inside(next1):
                x = next1[0]
            elif next2 not in visited and is_inside(next2):
                y = next2[1]
            else:
                print("Error! Both path options are already visited!")
                exit(1)
            continue
visited.append( (x, y) )
print("Loop completed. Distance:", round(distance/2))

# part 2
# check if point is inside polygon by checking amount of crossed paths
visited.append((start_x, start_y))

inside_points = []
count = 0
for y, row in enumerate(lines):
    prev_corner = None
    crossings = 0
    for x, col in enumerate(row):
        if (x, y) in visited:
            match col:
                case "|":
                    crossings += 1
                case "7":
                    if prev_corner == "L":
                        crossings += 1
                case "J":
                    if prev_corner == "F":
                        crossings += 1
                case "S":
                    crossings += 1
            if col != "-":
                prev_corner = col
        else:
            if crossings % 2 == 1:
                count += 1
                inside_points.append((x, y))



print("Total inside:", count)